import React, { useState } from 'react';
import Navbar from './components/Navbar';
import TodoForm from './components/TodoForm';
import TodoList from './components/TodoList';
import { ITodo } from './interfaces';
import './App.css';

declare var confirm: (question: string) => boolean;

const App: React.FC = () => {

  const [todos, setTodos] = useState<ITodo[]>([]);

  const addHandler = (title: string) => {
    let newTodo: ITodo = {
      title: title,
      id: Date.now(),
      completed: false
    }
    setTodos(prev => [newTodo, ...prev])
  }

  const toggleHandler = (id: number) => {
    setTodos(prev => prev.map(todo => {
      if (todo.id === id) {
        todo.completed = !todo.completed
      }
      return todo
    })
    )
  }

  const removeHandler = (id: number) => {
    const shouldRemove = confirm('Вы уверены что хотите удалить задание ?');

    if(shouldRemove){ 
      setTodos(prev => prev.filter(todo => todo.id !== id))
    }  
  }

  return (
    <>
      <Navbar />
      <div className='container'>
        <TodoForm onAdd={addHandler} />
        <TodoList
          todos={todos}
          onToggle={toggleHandler}
          onRemove={removeHandler}
        />
      </div>
    </>
  );
}

export default App;
